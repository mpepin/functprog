#!/bin/bash

function print_file {
  while read line; do
    echo "  $line"
  done < $1
  return 0
}

for file in bad/*.err; do
  if [ -n "$(diff $file $file.ref)" ]; then
    echo "-> Mismatch for ${file%err}f"
    echo "Expected:"
    print_file $file.ref
    echo "Got:"
    print_file $file
    echo
  fi
done
