open Atom
open Types
open Terms

(* ------------------------------------------------------------------------- *)

(* Specification of the type-checker. *)

(* The core of the typechecker is made up of two mutually recursive
   functions. [infer] infers a type and returns it; [check] infers a
   type, checks that it is equal to the expected type, and returns
   nothing. *)

(* The type [ty] that is produced by [infer] should be correct, that
   is, the typing judgement [hyps, tenv |- term : ty] should hold.
   Furthermore, it should be unique, that is, for every type [ty']
   such that the judgement [hyps, tenv |- term : ty'] holds, the
   hypotheses [hyps] entail the equation [ty = ty']. *)

(* The function [check] should be correct, that is, if it succeeds,
   then the typing judgement [hyps, tenv |- term : expected] holds. *)

(* ------------------------------------------------------------------------- *)

(* A completeness issue. *)

(* Ideally, the functions [infer] and [check] should be complete, that
   is, if they fail, then the term is ill-typed according to the
   typing rules in Pottier and Gauthier's paper. However, with the
   tools that we provide, this goal is difficult to meet, for the
   following reason.

   Consider, for instance, a function application [x1 x2]. We can use
   [infer] to obtain the types of [x1] and [x2], say, [ty1] and
   [ty2]. Then, we must check that, FOR SOME TYPE [ty], THE HYPOTHESES
   [hyps] ENTAIL THAT the type [ty1] is equal to the function type
   [TyArrow ty2 ty].

   This is a bit problematic. Of course, if the hypotheses [hyps] are
   empty, this is easy: it is just a syntactic check. If somehow the
   type [ty] was known, this would be easy as well: it would be an
   entailment check, for which we provide a decision procedure.
   However, in the general case, we need to solve a more difficult
   problem -- entailment with unknowns -- for which (out of laziness,
   perhaps) we haven't provided an algorithm.

   As a result, we suggest that you stick with a simple syntactic
   check. Your type-checker will thus be incomplete. That will not be
   a real problem: the user can work around it by adding an explicit
   type annotation to convert [ty1] into the desired function type
   [TyArrow ty1 ty]. The sample file [test/good/conversion.f]
   illustrates this.

   If you follow our suggestion and develop an incomplete
   type-checker, then you may run into a problem when implementing
   defunctionalization. The program produced by your
   defunctionalization phase may be well-typed, yet may be rejected by
   your type-checker, for the above reason. If this happens, you will
   have to work around the problem by having your defunctionalization
   algorithm produce explicit type annotations where appropriate. *)

(* ------------------------------------------------------------------------- *)

(* Fills the foralls at the beginning of scheme with the types [alphas].
   Also checks that there are as many nested foralls in [scheme] as types in
   [alphas] *)

let fill_scheme
    (xenv : Export.env)    (* for error reporting *)
    (loc : Error.location) (* for error reporting *)
    (dc : atom)            (* data constructor, for error reporting *)
    (scheme : ftype)       (* the type scheme to be filled *)
    (alphas : ftype list)  (* the types to be filled into the type scheme *)
    : ftype =
  (* Arity check *)
  let nb_args = List.length alphas in
  let nb_foralls = Types.count_foralls scheme in
  if nb_args <> nb_foralls then
    Typerr.arity_mismatch xenv loc
      "data constructor" dc
      "type" nb_foralls nb_args;
  (* The multiple fills *)
  let fill_one abs ty = match abs with
    | TyForall ctx -> fill ctx ty
    | _ -> assert false
  in List.fold_left fill_one scheme alphas

(* Takes a scheme without foralls, e.g. something like
   [TyWhere(
      TyWhere(
        TyArrow(TyTuple(...), TyCon(...)),
        type,
        type
      ),
      type,
      type
    )]
  and deconstruct it. Returns
  - the argument types
  - the type constructor
  - the resulting type
  - the hypotheses
*)

let rec deconstruct_data
    (xenv : Export.env)
    (loc : Error.location)
    : ftype -> ftype list * atom * ftype list * equations
    = function
  | TyWhere (ty1, ty2, ty3) ->
      let tuple, tc, restys, data_hyps = deconstruct_data xenv loc ty1 in
      tuple, tc, restys, (ty2, ty3)::data_hyps
  | ty ->
      let dom, codom = Typerr.deconstruct_arrow xenv loc ty in
      let tc, restys = Typerr.deconstruct_tycon xenv loc codom in
      Typerr.deconstruct_tuple xenv loc dom, tc, restys, []

(* The type-checker. *)

let rec infer              (* [infer] expects... *)
    (p : program)          (* a program, which provides information about type & data constructors; *)
    (xenv : Export.env)    (* a pretty-printer environment, for printing types; *)
    (loc : Error.location) (* a location, for reporting errors; *)
    (hyps : equations)     (* a set of equality assumptions *)
    (tenv : tenv)          (* a typing environment; *)
    (term : fterm)         (* a term; *)
    : ftype =              (* ...and returns a type. *)

  match term with
  | TeVar atm ->
      Types.lookup atm tenv

  | TeAbs (param, ty, body, fun_info) as term ->
      let xenv = Export.bind xenv param in
      (* check that dom(tenv) = fv(λx:ty.body) *)
      let tenv = AtomSet.fold
        (fun atm new_env -> bind atm (lookup atm tenv) new_env)
        (Symbols.fv term)
        Types.empty in
      let fty = TyArrow(
        ty,
        infer p xenv loc hyps (bind param ty tenv) body
      ) in
      fun_info := Some { hyps = hyps ; tenv = tenv ; fty = fty };
      fty

  | TeApp (f, x, app_info) ->
      let tf = infer p xenv loc hyps tenv f in
      let dom_ty, codom_ty = Typerr.deconstruct_arrow xenv loc tf in
      check p xenv hyps tenv x dom_ty;
      app_info := Some { domain = dom_ty ; codomain  = codom_ty };
      codom_ty

  | TeLet (x, e1, e2) ->
      let ty1 = infer p xenv loc hyps tenv e1 in
      let ty2 = infer p xenv loc hyps (bind x ty1 tenv) e2 in
      ty2

  | TeFix (x, ty, e) ->
      check p xenv hyps (bind x ty tenv) e ty;
      ty

  | TeTyAbs (atm, e) ->
      let xenv = Export.bind xenv atm in
      let ty = infer p xenv loc hyps tenv e in
      TyForall (abstract atm ty)

  | TeTyApp (e, ty) ->
      let inferred = infer p xenv loc hyps tenv e in
      let schema = Typerr.deconstruct_univ xenv loc inferred in
      fill schema ty

  | TeData (atm, tys, terms) ->
      (* atm :: forall alphas [D].{ls:taus} -> T taus1 *)
      let scheme = Symbols.type_scheme p atm in
      let data_ty = fill_scheme xenv loc atm scheme tys in
      let tuple, tc, restys, data_hyps = deconstruct_data xenv loc data_ty in
      (* C ||- [alphas -> tys] D *)
      if not @@ Equations.entailment hyps data_hyps then
        Typerr.unsatisfied_equations xenv loc hyps data_hyps;
      (* C,tenv |- es : tuple *)
      let nb_terms = List.length terms in
      let expected = List.length tuple in
      if nb_terms <> expected then
        Typerr.arity_mismatch xenv loc
          "data constructor" atm
          "term" expected nb_terms;
      List.iter2 (check p xenv hyps tenv) terms tuple;
      (* C,tenv |- atm tys { ls = terms } : atm [alphas -> tys] taus1 *)
      TyCon (tc, restys)

  | TeTyAnnot (e, ty) ->
      check p xenv hyps tenv e ty;
      ty

  | TeMatch (e, ty, clauses) ->
      (* C,Env |- e : termty *)
      let termty = infer p xenv loc hyps tenv e in
      let tc, exptys = Typerr.deconstruct_tycon xenv loc termty in
      (* for all clause c: C,Env |- c : tc -> ty *)
      let dcset = List.fold_left
        (check_clause p xenv hyps tenv (tc, exptys) ty)
        (Symbols.data_constructors p tc)
        clauses in
      (* Check the exhaustiveness of the pattern matching *)
      let unreachable dc =
        let scheme = Symbols.type_scheme p dc in
        let freevars = Array.make
          (Types.count_foralls scheme)
          dc in
        let freevars = Array.to_list freevars in
        let freevars = List.map (fun atm -> Atom.fresha atm) freevars in
        let alphas = List.map (fun atm -> TyFreeVar atm) freevars in
        let xenv = Export.sbind xenv freevars in
        let _, _, restys, data_hyps =
          deconstruct_data xenv loc
          (fill_scheme xenv loc dc scheme alphas) in
        let hyps = hyps @ data_hyps @ (List.combine exptys restys) in
        if not (Equations.inconsistent hyps) then
          Typerr.missing_clause xenv hyps loc dc in
      AtomSet.iter unreachable dcset;
      (* Resulting type *)
      ty

  | TeLoc (loc', e) ->
      infer p xenv loc' hyps tenv e

and check_clause
    (p : program)
    (xenv : Export.env)
    (hyps : equations)
    (tenv : tenv)
    ((expected_tc, exp_tys) : atom * ftype list)
    (expected_codom : ftype)
    (dcset : AtomSet.t)
    : clause -> AtomSet.t
    = function Clause (PatData (loc, dc, typs, vars), term) ->
  let xenv = Export.sbind xenv typs in
  (* Check if the pattern matching is redundant *)
  if not (AtomSet.mem dc dcset) then
    Typerr.redundant_clause loc;
  (* Get the type scheme corresponding to data constructor dc *)
  let scheme = Symbols.type_scheme p dc in
  let typs = List.map (fun atm -> TyFreeVar atm) typs in
  let tuple, tc, restys, data_hyps =
    deconstruct_data xenv loc
    (fill_scheme xenv loc dc scheme typs) in
  (* Check that this data constructor has the expected (algebraic data) type *)
  if not (Atom.equal tc expected_tc) then
    Typerr.typecon_mismatch xenv loc dc expected_tc tc;
  (* Add the equality between type variables and concrete types in the
     hypotheses. *)
  let hyps = hyps @ data_hyps @ (List.combine exp_tys restys) in
  (* Check if this clause is dead code *)
  if Equations.inconsistent hyps then
    Typerr.inaccessible_clause loc;
  (* Bind the variables introduced by the pattern in tenv *)
  let nb_vars = List.length vars in
  let nb_expected = List.length tuple in
  if nb_vars <> nb_expected then
    Typerr.arity_mismatch xenv loc
      "data constructor" dc
      "term" nb_expected nb_vars;
  let tenv =
    List.fold_left2 (fun env var ty -> bind var ty env) tenv vars tuple in
  (* Check the type of the body of the clause *)
  check p xenv hyps tenv term expected_codom;
  (* Removes the data constructor from the set *)
  AtomSet.remove dc dcset

and check                  (* [check] expects... *)
    (p : program)          (* a program, which provides information about type & data constructors; *)
    (xenv : Export.env)    (* a pretty-printer environment, for printing types; *)
    (hyps : equations)     (* a set of equality assumptions *)
    (tenv : tenv)          (* a typing environment; *)
    (term : fterm)         (* a term; *)
    (expected : ftype)     (* an expected type; *)
    : unit =               (* ...and returns nothing. *)

  (* We bet that the term begins with a [TeLoc] constructor. This should be
     true because the parser inserts one such constructor between every two
     ordinary nodes. In fact, this is not quite true, because the parser
     sometimes expands syntactic sugar without creating intermediate [TeLoc]
     nodes. If you invoke [check] in reasonable places, it should just work. *)

  match term with
  | TeLoc (loc, term) ->
      let inferred = infer p xenv loc hyps tenv term in
      if not (Equations.entailment hyps [inferred, expected]) then
        Typerr.mismatch xenv loc hyps expected inferred

  | _ ->
      (* out of luck! *)
      assert false

(* ------------------------------------------------------------------------- *)

(* A complete program is typechecked within empty environments. *)

let run (Prog (tctable, dctable, term) as p : program) =
  let xenv = Export.empty
  and loc = Error.dummy
  and hyps = []
  and tenv = Types.empty in
  let xenv = AtomMap.fold (fun tc _ xenv -> Export.bind xenv tc) tctable xenv in
  let xenv = AtomMap.fold (fun dc _ xenv -> Export.bind xenv dc) dctable xenv in
  xenv, infer p xenv loc hyps tenv term
