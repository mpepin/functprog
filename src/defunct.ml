open Terms
open Types

(* A record type for the data concerning the apply function we will have to deal
   with in the translate_* functions *)
type app_data = {
  tc : Atom.atom ;
  (* The type constructor Arrow *)

  f : Atom.atom ; 
  (* The variable atom "apply" *)

  clauses : (Atom.atom * ftype * clause, unit) Hashtbl.t
  (* A hash table used as a mutable set for storing the data used to build
     apply's clauses:
     - The parameter of the lambda
     - It's type (we annotate the parameter in the clause in order to help the
       typechecker type apply)
     - The clause *)
}

(* Fresh atoms for the the data constructor of the type arrow *)
let fresh_dc =
  let i = ref (-1) in
  function () ->
    incr i ; 
    let label = Printf.sprintf "ARROW__%d" !i in
    Atom.fresh (Identifier.mk label (3, label))


(* A functionnal tool to apply our translations functions of
   type [a -> b -> a * b] on lists *)
let foldmap f l start =
  let g x (y, xs) =
    let y', x' = f y x in
    y', x' :: xs in
  List.fold_right g l start

(* Don't wanna write it twice *)
let get : 'a option -> 'a = function
  | None -> assert false
  | Some x -> x

(* Don't wanna write it twice *)
let deconstruct_arrow = function
  | TyArrow (dom, codom) ->
      dom, codom
  | _ ->
      (* Should not happen at this point *)
      assert false

(* Translate a type by replacing all the arrows [a -> b] by the new arrow type
   [arrow a b] *)
let rec translate_type (arrow : Atom.atom) : ftype -> ftype = function
  | TyBoundVar _ ->
      (* Should not happen *)
      assert false
  | TyFreeVar atm ->
      TyFreeVar atm
  | TyArrow (dom, codom) ->
      let dom = translate_type arrow dom in
      let codom = translate_type arrow codom in
      TyCon (arrow, [dom; codom])
  | TyForall ctxt ->
      let alpha = Atom.fresh (hint ctxt) in
      let ty = fill ctxt (TyFreeVar alpha) in
      TyForall (abstract alpha @@ translate_type arrow ty)
  | TyCon (tc, tys) ->
      TyCon (tc, List.map (translate_type arrow) tys)
  | TyTuple tys ->
      TyTuple (List.map (translate_type arrow) tys)
  | TyWhere (ty1, ty2, ty3) ->
      TyWhere (
        translate_type arrow ty1,
        translate_type arrow ty2,
        translate_type arrow ty3
      )

(* Translate a well-formed type scheme. There is an arrow that need not to be
   translated in it *)
(* Assume the type scheme is well-formed *)
let rec translate_scheme (arrow : Atom.atom) : ftype -> ftype = function
  | TyForall ctxt ->
      let alpha = Atom.fresh (hint ctxt) in
      let ty = fill ctxt (TyFreeVar alpha) in
      TyForall (abstract alpha @@ translate_scheme arrow ty)
  | TyWhere (ty, ty1, ty2) ->
      TyWhere (
        translate_scheme arrow ty,
        translate_type arrow ty1,
        translate_type arrow ty2
      )
  | TyArrow (dom, codom) ->
      TyArrow (translate_type arrow dom, translate_type arrow codom)
  | _ -> failwith "Ill-formed type scheme"

(* Translate a term removing all the lambda abstractions. The only interesting
   rules are TeAbs and TeApp, the others are simple recursive calls. One assumes
   the term is well-typed and that the necessary information about the
   abstractions and applications are provided.
   
   Some stuff about the arrow type and the apply function need to be known (see
   {!app_data} above), that's the [app] argument.
   
   [dctable] contains all the data constructor known by the defunctionalizer at
   this point, it has to be updated each time a new data constructor is made
   e.g. each type a lambda abstraction in translated.
*)
let rec translate_term
    (app : app_data)
    (dctable : datacon_table)
    : fterm ->
      datacon_table * fterm
    = function
  | TeVar var ->
      dctable, TeVar var

  | TeAbs (param, ty, body, fun_info) ->
      (***
       * Get information from [fun_info]
       ***)

      let fun_info = get !fun_info in
      (* Extract the typing env *)
      let envvars, envtys = List.split @@ extract fun_info.tenv in
      let envtys = List.map (translate_type app.tc) envtys in
      (* Extract the domain and codomain of the abstraction *)
      let dom, codom = deconstruct_arrow fun_info.fty in
      let dom = translate_type app.tc dom in
      let codom = translate_type app.tc codom in
      
      (***
       * Build the type scheme of the new data constructor
       ***)

      (* The hypotheses *)
      let hyps = List.map
        (fun (ty1, ty2) ->
          translate_type app.tc ty1, translate_type app.tc ty2)
        fun_info.hyps in
      (* The foralls *)
      let alphas = Atom.AtomSet.union (ftv dom) (ftv codom) in
      let alphas = List.fold_left
        (fun alphas (ty1, ty2) -> Atom.AtomSet.(
          union (ftv ty1) (union (ftv ty2) alphas)))
        alphas
        hyps in
      let alphas = List.fold_left
        (fun alphas ty -> Atom.AtomSet.union (ftv ty) alphas)
        alphas
        envtys in
      let alphas = Atom.AtomSet.union alphas (Symbols.te_ftv body) in
      let alphas = Atom.AtomSet.elements alphas in
      (* Finally the type scheme *)
      let scheme = foralls
        alphas
        (wheres
          (TyArrow (TyTuple envtys, TyCon (app.tc, [dom; codom])))
          hyps) in
      (* Add the new data constructor to [dctable] *)
      let dc = fresh_dc () in
      let dctable = Atom.AtomMap.add dc scheme dctable in

      (***
       * Add a new clause for the apply function
       ***)

      let dctable, body = translate_term app dctable body in
      let clause = Clause (
        PatData (Error.dummy, dc, alphas, envvars),
        body
      ) in
      let ty = translate_type app.tc ty in
      Hashtbl.add app.clauses (param, ty, clause) ();

      (***
       * Build the resulting term
       ***)

      let envvars = List.map (fun atm -> TeVar atm) envvars in
      let alphas = List.map (fun atm -> TyFreeVar atm) alphas in
      let term = TeData (dc, alphas, envvars) in

      (***
       * And finally return
       ***)

      dctable, term

  | TeApp (f, x, app_info) ->
      (* Translate types *)
      let app_info = get !app_info in
      let dom = translate_type app.tc app_info.domain in
      let codom = translate_type app.tc app_info.codomain in
      (* Translate terms *)
      let dctable, f = translate_term app dctable f in
      let dctable, x = translate_term app dctable x in
      (* Build the resulting term *)
      let term = TeTyApp (TeVar app.f, dom) in
      let term = TeTyApp (term, codom) in
      let term = TeApp (term, f, ref None) in
      let term = TeApp (term, x, ref None) in
      (* Return *)
      dctable, term

  | TeLet (atm, x, y) ->
      let dctable, x = translate_term app dctable x in
      let dctable, y = translate_term app dctable y in
      dctable, TeLet (atm, x, y)

  | TeFix (atm, ty, term) ->
      let dctable, term = translate_term app dctable term in
      let ty = translate_type app.tc ty in
      dctable, TeFix (atm, ty, term)

  | TeTyAbs (atm, term) ->
      let dctable, term = translate_term app dctable term in
      dctable, TeTyAbs (atm, term)

  | TeTyApp (term, ty) ->
      let dctable, term = translate_term app dctable term in
      let ty = translate_type app.tc ty in
      dctable, TeTyApp (term, ty)

  | TeData (atm, tys, terms) ->
      let tys = List.map (translate_type app.tc) tys in
      let dctable, terms =
        foldmap (translate_term app) terms (dctable, []) in
      dctable, TeData (atm, tys, terms)

  | TeTyAnnot (term, ty) ->
      let dctable, term = translate_term app dctable term in
      let ty = translate_type app.tc ty in
      dctable, TeTyAnnot (term, ty)

  | TeMatch (term, ty, match_clauses) ->
      let dctable, term = translate_term app dctable term in
      let ty = translate_type app.tc ty in
      let dctable, match_clauses =
        foldmap (translate_clause app) match_clauses (dctable, []) in
      dctable, TeMatch (term, ty, match_clauses)

  | TeLoc (loc, term) ->
      let dctable, term = translate_term app dctable term in
      dctable, TeLoc (loc, term)

(* Inductively translate a clause *)
and translate_clause app dctable (Clause (pat, term)) =
  let dctable, term = translate_term app dctable term in
  dctable, Clause (pat, term)


(* This function generates the code of the apply function once the whole program
   has been translated and thus the bodys of all the lambda abstractions have
   been stored in [app.clauses] *)
let generate_apply (app : app_data) =
  (* The type of apply *)
  let alpha1 = Atom.fresh (Identifier.mk "app_a" Syntax.type_sort) in
  let alpha2 = Atom.fresh (Identifier.mk "app_b" Syntax.type_sort) in
  let arrow_ty = TyCon (
    app.tc,
    [TyFreeVar alpha1; TyFreeVar alpha2]
  ) in
  let ty = foralls
    [alpha1; alpha2]
    (arrows [arrow_ty; TyFreeVar alpha1] (TyFreeVar alpha2)) in
  (* The two arguments of the apply function *)
  let f_id = Atom.fresh (Identifier.mk "app_f" Syntax.term_sort) in
  let arg_id = Atom.fresh (Identifier.mk "app_arg" Syntax.term_sort) in
  (* Build the clauses *)
  let clauses = Hashtbl.fold
    (fun (param, ty, Clause (pat, term)) () clauses ->
      let annot_var = TeTyAnnot (TeVar arg_id, ty) in
      Clause (pat, TeLet (param, annot_var, term)) :: clauses)
    app.clauses 
    [] in
  (* Build the function itself *)
  let term = TeMatch (TeVar f_id, TyFreeVar alpha2, clauses) in
  let term = TeAbs (arg_id, TyFreeVar alpha1, term, ref None) in
  let term = TeAbs (f_id, arrow_ty, term, ref None) in
  let term = TeTyAbs (alpha1, TeTyAbs (alpha2, term)) in
  let term = TeFix (app.f, ty, term) in
  term


(* The main function only instantiates [app], translates the whole program, add
   the apply function and returns the updated program *)
let translate (Prog (tctable, dctable, term) : program) =
  (* Add the Arrow type constructor *)
  let app = {
    tc = Atom.fresh (Identifier.mk "__arrow" Syntax.typecon_sort) ;
    f = Atom.fresh (Identifier.mk "apply" Syntax.term_sort) ;
    clauses = Hashtbl.create 17
  } in
  let tctable = Atom.AtomMap.add app.tc 2 tctable in
  (* Translate the program code *)
  let dctable = Atom.AtomMap.map (translate_scheme app.tc) dctable in
  let dctable, term = translate_term app dctable term in
  (* Add the apply function to the code *)
  let apply = generate_apply app in
  let code = TeLet (app.f, apply, term) in
  (* Return the program *)
  Prog (tctable, dctable, code)
